# Smart Kitchen Trash Classifier

    python retrain.py --image_dir DataSet --how_many_training_steps 500 --model_dir=inception --output_graph=retrained_graph.pb --output_labels=retrained_labels.txt


For test

    python label_image.py --graph=retrained_graph.pb --labels=retrained_labels.txt --input_layer=Placeholder --output_layer=final_result --image=/home/youssef/DataSet/meat/bones.jpg

